# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Ce document a pour but de décrire les objectifs et limites du projet réalisé en binôme par Latisha Walter et Franck Jude dans le cadre du cours de Web Services. On s’y efforcera d’y détailler les contours et limites du projet, et ses spécificités.

### How do I get set up? ###

Le projet sera documenté et ses sources mises en lignes afin de facilité son accès et sa compréhension ainsi que pour permettre un développement souple et collaboratif.

GIT : https://bitbucket.org/freaknarf/ws-dagpi
Google Doc : https://drive.google.com/drive/folders/0BxnUoJcVwrUnV2ZweG04NUdScHM


### Who do I talk to? ###

Latisha Walter <latisha.walter@etu.u-bordeaux.fr>
Franck Jude <franck.jude@etu.u-bordeaux.fr>